//
//  main.c
//  prob3
//
//  Created by Farid on 10/30/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // determine the values are even or odd
    int a, b, c=0;
    scanf("%d", &b);
    
    for (a=1; a<=b; a++) {
        scanf("%d", &c);
        if (c==0) {
            continue;
        } else if (c%2==0) {
            printf("Even \n");
            continue;
        } else {
            printf("Odd \n");
            continue;
        }
    }
    
    
    return 0;
}
