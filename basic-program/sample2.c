//
//  main.c
//  prob4
//
//  Created by Farid on 10/26/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main() {
    // insert code here...
    int a, b, c = 1;
    scanf("%d", &b);
    
    for (a = 1; a<=b; a++) {
        c = c * a;
        printf("%d X %d = %d \n", a, b, c);
    }
    printf("Total = %d \n", c);
    
    return 0;
}
