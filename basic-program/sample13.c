//
//  main.c
//  lab6
//
//  Created by Farid on 11/20/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    
    int a, n, i, sum1=0, sum2=0, sum3=0;
    printf("1. Mutiplication Table\n");
    printf("2. Factorial Value\n");
    printf("3. Square Value\n");
    printf("4. Exit\n");
    
    while ('true') {
        scanf("%d", &n);
        if (n==1) {
            scanf("%d", &a);
            printf("Mutiplication Value is:\n");
            for (i=1; i<=10; i++) {
                sum1 = a * i;
                printf("%d X %d = %d\n", a, i, sum1);
            }
        } else if (n==2) {
            scanf("%d", &a);
            for (i=1; i<=a; i++) {
                sum2 += i;
            }
            printf("Factorial Value is: %d\n", sum2);
        } else if (n==3) {
            scanf("%d", &a);
            for (i=1; i<=a; i++) {
                sum3 = i*i;
            }
            printf("Square Value is: %d\n", sum3);
        } else if (n==4) {
            break;
        }
    }
    
    return 0;
}
