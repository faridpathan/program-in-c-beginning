//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int a, b; // Declare two Intager
    scanf("%d %d", &a, &b); // Just input form keyboard that value
    
    /* if your input is greater than b then show first one and otherwise show last one */
    if (a>b) {
        printf("A is greater than B = %d\n", a);
    } else {
        printf("B is greater than A = %d\n", b);
    }
    
    
    return 0;
}
