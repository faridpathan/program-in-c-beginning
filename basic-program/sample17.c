//
//  main.c
//  max-value-in-array
//
//  Created by Farid on 12/18/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // insert code here...
    
    int i, ar[100], n, max;
    scanf("%d", &n);
    for (i=0; i<n; i++) {
        scanf("%d", &ar[i]);
    }
    max = ar[0];
    
    for (i=1; i<n; i++) {
        if (ar[i] > max) {
            max = ar[i];
        }
    }
    printf("%d\n", max);
    
    for (i=0; i<n; i++) {
        if (ar[i] < max) {
            max = ar[i];
        }
    }
    printf("%d\n", max);
    
    return 0;
}
