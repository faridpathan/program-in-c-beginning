//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int i, a, sum=0; // Declare one Intager
    scanf("%d", &a); // Just input form keyboard that value
    
    /* 
        Simple for loop statements find Cube Series Vale
    */
    
    for (i=1; i<=a; i++) {
        sum += i*i*i;
    }
    printf("Cube Series %d\n", sum);
    
    
    return 0;
}

/*
OUTPUT
 
 4
 Simple Series 100
 Program ended with exit code: 0
 
*/
