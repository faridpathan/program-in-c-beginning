//
//  main.c
//  class7
//
//  Created by Farid on 12/3/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // insert code here...
    
    int n, i, bArray[100], m=0, arv=0, tb=0;
    scanf("%d", &n);
    for (i=0; i<n; i++) {
        scanf("%d", &bArray[i]);
        if (bArray[i]%2 == 0) {
            printf("Even Number is = %d\n", bArray[i]);
        } else {
            printf("Odd Number is = %d\n", bArray[i]);
        }
        
        m += bArray[i];
    }
    arv = m/n;
    printf("Average value is: %d\n", arv);
    
    for (i=1; i<=10; i++) {
        tb = i*bArray[n-1];
        printf("%d X %d = %d\n", bArray[n-1], i, tb);
    }
    
    
    return 0;
}
