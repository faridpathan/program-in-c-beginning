//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int a; // Declare one Intager
    scanf("%d", &a); // Just input form keyboard that value
    
    /* 
        if your input any value then check and this value is Even so print Even otherwise print Odd
    */
    if (a%2 == 0) {
        printf("Even Value = %d\n", a);
    } else {
        printf("Odd Value = %d\n", a);
    }
    
    
    return 0;
}
