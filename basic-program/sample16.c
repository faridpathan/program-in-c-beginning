//
//  main.c
//  lab9a
//
//  Created by Farid on 12/11/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // insert code here...
    
    int n;
    scanf("%d", &n);
    int ar[n], i;
    
    for (i=0; i<n; i++) {
        scanf("%d", &ar[i]);
    }
    
    if (n%2 == 0) {
        for (i=0; i<n; i++) {
            if (n/2==i) {
                continue;
            }
            printf("%d", ar[i]);
        }
        printf("\n");
    } else {
        for (i=0; i<n; i++) {
            if (n/2==i) {
                continue;
            }
            printf("%d", ar[i]);
        }
        printf("\n");
    }
    
    
    return 0;
}

/*
 
 5
 2 6 9 8 7
 
*/
