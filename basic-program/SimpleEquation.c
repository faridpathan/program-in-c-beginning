//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int a, b, c; // Declare two Intager
    a = 5; // define default value 5
    b = 7; // define default value 7
    
    c = a + b; // Just simple put for Addition the value
    printf("The value is: %d\n", c);
    
    c = b - a; // Just simple put for Subtraction that value
    printf("The value is: %d\n", c);
    
    c = a * b; // Just simple put for Multiplication that value
    printf("The value is: %d\n", c);
    
    float d = b / a; // Just simple put for Difference that value
    printf("The value is: %f\n", d);
    
    
    return 0;
}
