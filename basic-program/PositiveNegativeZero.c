//
//  main.c
//  Class2
//
//  Created by Farid on 10/6/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    // Event And Odd
    int a;
    scanf("%d", &a);
    if (a%2 == 0) {
        printf("Event \n");
    }
    else {
        printf("Odd \n");
    }
    
    
    // Positive, Negative and Zero
//    int a;
//    scanf("%d", &a);
//    if (a > 0) {
//        printf("Positive \n");
//    }
//    else if (a < 0) {
//        printf("Negative \n");
//    }
//    else {
//        printf("Zero \n");
//    }
    
    
    int xa, ya, za;
    scanf("%d %d %d", &xa, &ya, &za);
    
    if ((xa>ya) && (xa>za)) {
        printf("1st Is The Maximum Number = %d \n", xa);
    } else if ((ya>xa) && (ya>za)) {
        printf("2nd Is The Maximum Number = %d \n", ya);
    } else {
        printf("3rd Is The Maximum Number = %d \n", za);
    }
    
    int xa, ya;
    scanf("%d %d", &xa, &ya);
    if (xa > ya) {
        printf("1st Is The Maximum Number = %d \n", xa);
    } else {
        printf("2nd Is The Maximum Number = %d \n", ya);
    }
    
    return 0;
}
