//
//  main.c
//  program3
//
//  Created by Farid on 11/8/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // insert code here...
    int n, m, t=0;
    scanf("%d", &n);
    while (n>0) {
        m = n%10;
        printf("%d", m);
        t = t+m;
        n = n/10;
    }
    printf("\n");
    
    return 0;
}
