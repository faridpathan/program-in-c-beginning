//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int i, a, sum=1; // Declare one Intager
    scanf("%d", &a); // Just input form keyboard that value
    
    /* 
        Simple for loop statements find Factorial Vale
    */
    
    for (i=1; i<=a; i++) {
        sum *= i;
    }
    printf("Summation all values in that value %d\n", sum);
    
    
    return 0;
}

/*
OUTPUT
 
 4
 Summation all values in that value 24
 Program ended with exit code: 0
 
*/
