//
//  main.c
//  prob-e1
//
//  Created by Farid on 10/31/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    int n, m, sum=0;
    scanf("%d", &n);
    
    while (n>0) {
        m = n % 10;
        printf("%d", m);
        sum += m;
        n = n/10;
    }
    printf("\n%d\n", sum);
    
    return 0;
}
