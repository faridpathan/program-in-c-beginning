//
//  main.c
//  1. SimplePlus
//
//  Created by Farid on 11/23/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // Plus in two input value
    
    int a, m, sum=0; // Declare one Intager
    scanf("%d", &a); // Just input form keyboard that value
    
    /* 
        Simple While statements
    */
    
    while (a>0) {
        m = a % 10;
        printf("%d", m);
        sum += m;
        a = a/10;
    }
    printf("\nSumetion value %d\n", sum);
    
    
    return 0;
}
