//
//  main.c
//  program2
//
//  Created by Farid on 11/2/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // summation value of the square and the cube series
    int a, b, c=0, d=0;
    scanf("%d", &b);
    
    for (a=1; a<=b; a++) {
        c += a*a;
        d += a*a*a;
    }
    printf("The summation of square series is = %d \n", c); // Square Series
    printf("The summation of cube series is = %d \n", d);   // Cube Series
    
    
    return 0;
}
