//
//  main.c
//  program1
//
//  Created by Farid on 11/1/16.
//  Copyright © 2016 Farid. All rights reserved.
//

#include <stdio.h>

int main() {
    // multiplication table and this factorial value
    int a, b, c=1;
    scanf("%d", &b);
    
    for (a=1; a<=b; a++) {
        c = c*a;
    }
    printf("The factorial of %d is: %d \n\n", b, c);
    
    printf("The multiplication table of %d is: \n", b);
    for (a=1; a<=10; a++) {
        c = b*a;
        printf("%d X %d = %d \n", a, b, c);
    }
    
    return 0;
}
