#Program in C
---
C is a programing language. C was originally developed by **Dennis Ritchie** between 1969 and 1973 at **Bell Labs** was a developer to used in **Unix operating system**.
---
C program file extention **".c"**. example *problem.c*

#C - Program Structure
---
A C program basically consists of the following parts --

* Preprocessor Commands
* Functions
* Variables
* Statements & Expressions
* Comments

Example of basic code in c-
```c
#include <stdio.h>

int main() {
   /* my first program in C */
   printf("Hello, World! \n");

   return 0;
}
```

#C - Data Type
---
They are arthmetic types and are further classified into: (A) integer types and (B) floating-point types.

##Integer types
---
Type | Storage Size
--- | ---
Char | 1 byte
int | 2 or 4 bytes
sort | 2 byte
long | 4 byte

##Floating Data Type
---
Type | Storage Size | Precision
--- | --- | ----
float | 4 byte | 6 decimal places
double | 8 byte | 15 decimal places
long double | 10 byte | 19 decimal places

#c - Variable
---
A variable means on kind of name or ID. some type of variable use int, char, float, double, bool etc. Variable can't support first any number then character exam"1var", any non char like(-,@,!,#,%,^,&,*, etc) it just support (_). Say some example about this...

```c
int i, i1, _i,__i, i_1; // use char or number and something like that but if you use -i, @i, 1i it's not support.
char c, ch; // any character support.
float f, salary;
double d;
```
*note: Variable difine condition both are same*

example
```c
#include <stdio.h>

// Variable declaration:
extern int a, b;
extern int c;
extern float f;

int main () {

   /* variable definition: */
   int a, b;
   int c;
   float f;

   /* actual initialization */
   a = 10;
   b = 20;

   c = a + b;
   printf("value of c : %d \n", c);

   f = 70.0/3.0;
   printf("value of f : %f \n", f);

   return 0;
}
```
Output
```
value of c : 30
value of f : 23.333334
```
*note: Int support complete number like **30** and float or double support Remainder number like 23.333334*

#C - Strings
---
String means some non functional text and any kind of text if you use inside double quotation ("") that will a string value. The following declaration and initialization create a string consisting of the word "Hello". To hold the null character at the end of the array, the size of the character array containing the string is one more than the number of characters in the word "Hello."
```c
#include <stdio.h>

int main () {

   char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
   printf("Greeting message: %s\n", greeting );
   return 0;
}
```
Output
```
Greeting message: Hello
```
Inside of print Greeting message use inside "" that's why it's string value. and Hello use char function but when it declear inside "" then it also string. so any inside "" value is string.

#Data Types Description
---
##*Integer*
Integer is a data type and it is a whole number (not a fractional number) that can be positive, negative or zero.
Examples of integers are: -5, 1, 5, 8, 97, and 3,043
if your need to declare any complete number you can use integer.
Example
```c
#include <stdio.h>

// Variable declaration:
extern int a, b;
extern int c;

int main () {

   /* variable definition: */
   int a, b;
   int c;

   /* actual initialization */
   a = 10;
   b = 20;

   c = a + b;
   printf("value of c : %d \n", c);

   return 0;
}
```

##*Float*
Float is a data type what is fractinal negative, positive number.
Example Float Number = 2.33, 2.3E, 0.3333.
Code Example
```c
#include <stdio.h>

// Variable declaration:
extern float f;

int main () {

   /* variable definition: */
   float f;

   f = 70.0/3.0;
   printf("value of f : %f \n", f);

   return 0;
}
```

##*Double*
Double and float almost same but double storage ability high and float is low. but almost same.
Example
```c
#include <stdio.h>

// Variable declaration:
extern float f;

int main () {

   /* variable definition: */
   double d;

   d = 70.0/3.0;
   printf("value of d : %f \n", d);

   return 0;
}
```

#C - Operations
---
An operator is a symble that tells the compiler to perform specific mathematical or logical functions.
Look into the operators:-

If declare two variable **A=10** and **B=20**
###Arithmetic Operators
Operator | Description | Example
--- | --- |---
+ | Adds two operators. | A + B = 30
- | Subtracts second operator from the first. | A - B = -10
* | Multiplies both operator | A * B = 200
/ | Divides Operator | B/A = 2
++ | Increment operator in integer value | A++ = 11
-- | Decrement operator in integer value | A-- = 9

###Relational Operator
Operator | Description | Example
--- | --- | ---
== | Checks if the values of two operands are equal or not. If yes, then the condition becomes true. | (A==B) is not true
!= | Checks if the values of two operands are equal or not. If the values are not equal, then the condition becomes true | (A != B) is true
> | Checks if the value of left operand is greater than the value of right operand. If yes, then the condition becomes true. | (A > B) is not true
< | Checks if the value of left operand is less than the value of right operand. If yes, then the condition becomes true. | (A < B) is true
>= | Checks if the value of left operand is greater than or equal to the value of right operand. If yes, then the condition becomes true. | (A >= B) is not true
<= | Checks if the value of left operand is less than or equal to the value of right operand. If yes, then the condition becomes true. | (A <= B) is true.

###Logical Operators
If two variable **A** holds 1 and variable **B** holds 0
Operator | Description | Example
--- | --- | ---
&& | Called Logical AND operator. If both the operands are non-zero, then the condition becomes true. | (A && B) is false.
|| | Called Logical OR Operator. If any of the two operands is non-zero, then the condition becomes true. | (A || B) is true.
! | Called Logical NOT Operator. It is used to reverse the logical state of its operand. If a condition is true, then Logical NOT operator will make it false. | !(A && B) is true

###Assignment Operator
Operator | Description | Example
--- | --- | ---
= | Simple assignment operator. Assigns values from right side operands to left side operand | C = A + B will assign the value of A + B to C
+= | Add AND assignment operator. It adds the right operand to the left operand and assign the result to the left operand. | C += A is equivalent to C = C + A
-= | Subtract AND assignment operator. It subtracts the right operand from the left operand and assigns the result to the left operand. | C -= A is equivalent to C = C - A
*= | Multiply AND assignment operator. It multiplies the right operand with the left operand and assigns the result to the left operand. | C *= A is equivalent to C = C * A
/= | Divide AND assignment operator. It divides the left operand with the right operand and assigns the result to the left operand. | C /= A is equivalent to C = C / A
%= | Modulus AND assignment operator. It takes modulus using two operands and assigns the result to the left operand. | C %= A is equivalent to C = C % A
<<= | Left shift AND assignment operator. | C <<= 2 is same as C = C << 2
>>= | Right shift AND assignment operator. | C >>= 2 is same as C = C >> 2
& | Bitwise AND assignment operator. | C &= 2 is same as C = C & 2
^= | Bitwise exclusive OR and assignment operator. | C ^= 2 is same as C = C ^ 2
|= | Bitwise inclusive OR and assignment operator. | C |= 2 is same as C = C | 2


#C - Decision Making
---
Decition making means just check true or false. if true then do something or false do another something.
like that this image:-
![Decition Making](http://themexcoder.com/cls/decision_making.jpg)

S.N | Statement & Description
--- | ---
1. | **if statement**
An **if statement** consists of a boolean expression followed by one or more statements.
2. | **if...else statement**
An **if statement** can be followed by an optional **else statement**, which executes when the Boolean expression is false.
3. | **nested if statements**
You can use one **if** or **else if** statement inside another if or else if statement(s).
4. | **switch statement**
A **switch statement** allows a variable to be tested for equality against a list of values.
5. | **nested switch statements**
You can use one **switch** statement inside another **switch** statement(s).

###if statement
The syntax of an 'if' statement in C programming language is −
```c
if(boolean_expression) {
   /* statement(s) will execute if the boolean expression is true */
}
```

**Flow Diagram**
![if statement Flow Diagram](http://themexcoder.com/cls/if_statement.jpg)

**Example**
```c
#include <stdio.h>

int main () {

   /* local variable definition */
   int a = 10;

   /* check the boolean condition using if statement */

   if( a < 20 ) {
      /* if condition is true then print the following */
      printf("a is less than 20\n" );
   }

   printf("value of a is : %d\n", a);

   return 0;
}
```
Output
```
a is less than 20;
value of a is : 10
```

For this example this is a condition **a < 20** that's mean this is true because 10 < 20. so it's true and that's why access this statement or if it's false can't access statement.

###if...else statement
The syntax of an if...else statement in C programming language is −
```c
if(boolean_expression) {
   /* statement(s) will execute if the boolean expression is true */
}
else {
   /* statement(s) will execute if the boolean expression is false */
}
```
**boolean_expression means any kind of conditions that's define **true** or **false**

**Flow Diagram**
![if else statement](https://www.tutorialspoint.com/cprogramming/images/if_else_statement.jpg)

**Example**
```c
#include <stdio.h>

int main () {

   /* local variable definition */
   int a = 100;

   /* check the boolean condition */
   if( a < 20 ) {
      /* if condition is true then print the following */
      printf("a is less than 20\n" );
   }
   else {
      /* if condition is false then print the following */
      printf("a is not less than 20\n" );
   }

   printf("value of a is : %d\n", a);

   return 0;
}
```

**Output**
```
a is not less than 20;
value of a is : 100
```

###If...else if...else Statement
An if statement can be followed by an optional **else if...else** statement, which is very useful to test various conditions using single if...else if statement.

**Synrax**
The syntax of an **if...else if...else** statement in C programming language is −
```
if(boolean_expression 1) {
   /* Executes when the boolean expression 1 is true */
}
else if( boolean_expression 2) {
   /* Executes when the boolean expression 2 is true */
}
else if( boolean_expression 3) {
   /* Executes when the boolean expression 3 is true */
}
else  {
   /* executes when the none of the above condition is true */
}
```

**Example**
```c
#include <stdio.h>

int main () {

   /* local variable definition */
   int a = 100;

   /* check the boolean condition */
   if( a == 10 ) {
      /* if condition is true then print the following */
      printf("Value of a is 10\n" );
   }
   else if( a == 20 ) {
      /* if else if condition is true */
      printf("Value of a is 20\n" );
   }
   else if( a == 30 ) {
      /* if else if condition is true  */
      printf("Value of a is 30\n" );
   }
   else {
      /* if none of the conditions is true */
      printf("None of the values is matching\n" );
   }

   printf("Exact value of a is: %d\n", a );

   return 0;
}
```
**Output**
```
None of the values is matching
Exact value of a is: 100
```

###nested if statements
It is always legal in C programming to nest if-else statements, which means you can use one if or else if statement inside another if or else if statement(s).

**Syntax**
```c
if( boolean_expression 1) {

   /* Executes when the boolean expression 1 is true */
   if(boolean_expression 2) {
      /* Executes when the boolean expression 2 is true */
   }
}
```

**Example**
```c
#include <stdio.h>

int main () {

   /* local variable definition */
   int a = 100;
   int b = 200;

   /* check the boolean condition */
   if( a == 100 ) {

      /* if condition is true then check the following */
      if( b == 200 ) {
         /* if condition is true then print the following */
         printf("Value of a is 100 and b is 200\n" );
      }
   }

   printf("Exact value of a is : %d\n", a );
   printf("Exact value of b is : %d\n", b );

   return 0;
}
```
**Output**
```
Value of a is 100 and b is 200
Exact value of a is : 100
Exact value of b is : 200
```

###switch statement
A switch statement allows a variable to be tested for equality against a list of values. Each value is called a case, and the variable being switched on is checked for each switch case.

**Syntax**
```c
switch(expression) {

   case constant-expression  :
      statement(s);
      break; /* optional */

   case constant-expression  :
      statement(s);
      break; /* optional */

   /* you can have any number of case statements */
   default : /* Optional */
   statement(s);
}
```

**Flow Diagram**
![switch statement](https://www.tutorialspoint.com/cprogramming/images/switch_statement.jpg)

**Example**
```c
#include <stdio.h>

int main () {

   /* local variable definition */
   char grade = 'B';

   switch(grade) {
      case 'A' :
         printf("Excellent!\n" );
         break;
      case 'B' :
      case 'C' :
         printf("Well done\n" );
         break;
      case 'D' :
         printf("You passed\n" );
         break;
      case 'F' :
         printf("Better try again\n" );
         break;
      default :
         printf("Invalid grade\n" );
   }

   printf("Your grade is  %c\n", grade );

   return 0;
}
```

**Output**
```
Well done
Your grade is B
```
